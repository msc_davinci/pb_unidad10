package org.davinci.aspectotres;

public class QuickSort {

        int partition(int arr[], int low, int high)
        {
            int pivot = arr[high];
            int i = (low-1); // index del elemento más pequeño
            for (int j=low; j<high; j++)
            {
                // Si el elemento actual es mas pequeño o igual al pivote
                if (arr[j] <= pivot)
                {
                    i++;

                    // intercambiar arr[i] y arr[j]
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }

            // intercambiar arr[i+1] y arr[high] (o pivot)
            int temp = arr[i+1];
            arr[i+1] = arr[high];
            arr[high] = temp;

            return i+1;
        }


        /* El método principal que implementa QuickSort()
        arr[] --> Arreglo a ser ordenado,
        low --> index de inicio,
        high --> index de fin*/
        void sort(int arr[], int low, int high)
        {
            if (low < high)
            {
			/* pi es indice de particionamiento, arr[pi] es
			es ahora el lugar correcto */
                int pi = partition(arr, low, high);

                // Ordenar recursivamente los elementos antes y después de la partición
                sort(arr, low, pi-1);
                sort(arr, pi+1, high);
            }
        }

        /* Una función de utilidad para imprimir matriz de tamaño n*/
        static void printArray(int arr[])
        {
            int n = arr.length;
            for (int i=0; i<n; ++i)
                System.out.print(arr[i]+" ");
            System.out.println();
        }

        public static void main(String args[])
        {
            int arr[] = {10, 7, 8, 9, 1, 5};
            int n = arr.length;

            QuickSort ob = new QuickSort();
            ob.sort(arr, 0, n-1);

            System.out.println("sorted array");
            printArray(arr);
        }

}
