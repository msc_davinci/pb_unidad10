package org.davinci.aspectodos;

public class Menor {
    private static int menorArreglo(int x[], int indice){
        int m = x.length-1;
        int menor, temp;
        if (indice==m){
            return menor = x[indice];
        }
        else{
            menor = menorArreglo(x, indice+1);
            if (x[indice]<menor){
                temp = x[indice];
            }else{
                temp = menor;
            }
        }
        return temp;
    }

    public static int numeroMenor(int x[]){
        return menorArreglo(x,0);
    }

    public static void main(String[] args) {
        int numeros[] = {8, 9, 5, 7, 10};
        System.out.println("El número menor es: " + numeroMenor(numeros));
    }
}
