package org.davinci.aspectouno;

public class Palindromo {
    public boolean probarPalindromo(String cadena, int i, int f){

        String cadena2="";

        //quitamos los espacios
        for (int x=0; x < cadena.length(); x++) {
            if (cadena.charAt(x) != ' ')
                cadena2 += cadena.charAt(x);
        }

        cadena = cadena2.toLowerCase();
        f = cadena.length();

        if (i<f) {
            if (cadena.charAt(i) == cadena.charAt(f-1)) {
                probarPalindromo(cadena, i + 1, f - 1);
                return true;
            }
        }
            return false;
    }
}
