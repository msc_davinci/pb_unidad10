package org.davinci.aspectouno;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner teclado= new Scanner(System.in);
        String nuevo;
        Palindromo palindromo = new Palindromo();
        System.out.println("Ingresa la palabra o frase:");
        nuevo = teclado.nextLine();
        System.out.println(palindromo.probarPalindromo(nuevo, 0, 0)?nuevo + " es Palindromo":nuevo + " No es Palindromo");
    }
}
